<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use LynX39\LaraPdfMerger\Facades\PdfMerger;
use PDF;
class HomeController extends Controller
{
    public function index()
    {
        return view('welcome');
    }

    function Header()
{
    // Logo
    $this->Image('logo.png',10,6,30);
    // Arial bold 15
    $this->SetFont('Arial','B',15);
    // Move to the right
    $this->Cell(80);
    // Title
    $this->Cell(30,10,'Title',1,0,'C');
    // Line break
    $this->Ln(20);
}

// Page footer
function Footer()
{
    // Position at 1.5 cm from bottom
    $this->SetY(-15);
    // Arial italic 8
    $this->SetFont('Arial','I',8);
    // Page number
    $this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
}
// https://manuals.setasign.com/fpdi-manual/v1/the-fpdi-class/examples/#index-2
    public function print(){
        // $pdf=PDFMerger::init();
        // // $path1 = public_path() . '/demo.pdf';
        // // $path2 = public_path() . '/demo2.pdf';

        // // $pdf->addPDF($path1, 'all');
        // $pdf1 = PDF::loadView('pdf');
        // $pdf1->save(public_path().'/filename.pdf');

        // $pdf->addPDF(public_path().'/filename.pdf');
        // $pdf->addPDF(public_path('demo2.pdf'),'all');
        // $pdf->addPDF(public_path().'/filename.pdf');
        // $pdf->addPDF(public_path('demo3.pdf'),'all');
        // $pdf->merge();

        // $pdf->save('merged.pdf','browser');

        
        $pdf1 = PDF::loadView('pdf');
        $pdf1->save(public_path().'/filename.pdf');

        $pdf2 = PDF::loadView('welcome');
        $pdf2->save(public_path().'/filename1.pdf');
        $pdf = new \FPDI();
        $pdf->AddPage();
        // set the source file
        $pdf->setSourceFile("filename.pdf");
        // import page 1
        $tplIdx = $pdf->importPage(1);
        // use the imported page and place it at point 10,10 with a width of 100 mm
        $pdf->useTemplate($tplIdx, 10, 10);
        
        $pageCount = $pdf->setSourceFile('demo3.pdf');

// iterate through all pages
for ($pageNo = 1; $pageNo <= $pageCount; $pageNo++) {

    // import a page
    
    $templateId = $pdf->importPage($pageNo);
    // get the size of the imported page
    $size = $pdf->getTemplateSize($templateId);

    // create a page (landscape or portrait depending on the imported page size)
    if ($size['w'] > $size['h']) {
        $pdf->AddPage('L', array($size['w'], $size['h']));
    } else {
        $pdf->AddPage('P', array($size['w'], $size['h']));
    }

    $pdf->SetFont('Arial','I',8);
    $pdf->Cell(30);

    // use the imported page

    $pdf->useTemplate($templateId);

    $pdf->SetY(-35);
    // Arial italic 8
    $pdf->SetFont('Arial','I',8);
    // Page number

    $pdf->Cell(0,14,'Page '.$pdf->PageNo(),0,0,'C');

}
$pdf->AddPage();
        // set the source file
        $pdf->setSourceFile("filename.pdf");
        // import page 1
        $tplIdx = $pdf->importPage(1);
        // use the imported page and place it at point 10,10 with a width of 100 mm
        $pdf->useTemplate($tplIdx, 10, 10);

// Output the new PDF
$pdf->Output();   
    }
}
